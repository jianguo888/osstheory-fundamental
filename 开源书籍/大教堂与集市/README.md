# 大教堂与集市

## 概述

《大教堂与集市》是开源运动的《圣经》，颠覆了传统的软件开发思路，影响了整个软件开发领域。作者Eric S. Raymond是开源运动的旗手、黑客文化第一理论家，他讲述了开源运动中惊心动魄的故事，提出了大量充满智慧的观念和经过检验的知识，给所有软件开发人员带来启迪。

英文原版

- http://www.catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar/

繁体基础版

- [大教堂与集市-繁体](大教堂与集市-繁体.md)

简体基础版

- [大教堂与集市-简体](大教堂与集市-简体.md)

## 致谢

感谢三位的翻译与整理。

- [远皓](https://github.com/thuwyh)
- [zwben](https://github.com/zwben)
- [crazyangelo](https://github.com/crazyangelo)

本文就是从以下两个地址摘录而来。

- https://github.com/thuwyh/The-Cathedral-the-Bazaar-zh

- https://github.com/crazyangelo/Cathedral-and-Bazaar

