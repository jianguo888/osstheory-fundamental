# 摘要

《维基经济学:大规模合作如何改变一切》是唐·塔普斯科特和安东尼·d·威廉姆斯合著的一本书，于2006年12月首次出版。该书探讨了21世纪初的一些公司是如何利用大规模合作和开源技术(如维基)取得成功的。

“维基经济学”一词描述了广泛合作和用户参与的影响，以及企业和市场之间的关系如何因此发生变化。

“维基经济学”一词描述了技术、人口统计和全球经济的结合，使大规模合作成为可能。尽管对某些人来说，大规模协作的复杂性似乎难以应对，但如果实施得当，它实际上是一种有益的商业和社会战略。

![输入图片说明](https://foruda.gitee.com/images/1669188993496434383/ba8b2a01_8894319.jpeg "Wiki.jpeg")

大规模合作不仅可以推动创新，帮助企业成长，还可以成为改变社会运行方式的催化剂。

事实上，大规模合作是新经济的引擎，因此对于那些能够理解和使用它的人来说，它可以成为一件强大的武器，而对于那些不能理解和使用它的人来说，它则是棺材上的一颗钉子。

事实是，整个商业的未来取决于企业如何适应“维基经济学”。随着消费者不断要求更高质量的产品，他们也希望更多地参与到这些产品的创造中。员工也希望有一定的自由和自主权。

这些变化需要企业的关注和反应，产生新的战略来管理他们的商品、劳动力和财产。

在这本书的总结中，我们学习如何有效地利用大规模合作的力量，通过观察积极的例子，开放合作可以增加产品的价值，从而使任何公司成为一个成功的、创新的公司。

在唐·塔普斯科特和安东尼·d·威廉姆斯对维基经济学的总结中，这本书的总结将向你展示:

- 如何通过分享你的信息把你的竞争对手变成合作者。
- 一家企业如何从货架上的灰尘中赚得盆满钵满。
- 为什么一家公司付钱让员工做他们想做的事。
- 以及为什么集中生产已经过时了。

# [维基经济学的核心理念](https://gitee.com/openatom-university/osstheory-fundamental/blob/master/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E7%BB%B4%E5%9F%BA%E7%BB%8F%E6%B5%8E%E5%AD%A6/%E7%BB%B4%E5%9F%BA%E7%BB%8F%E6%B5%8E%E5%AD%A6%E6%A0%B8%E5%BF%83%E7%90%86%E5%BF%B5.md)

# [宏观维基经济学摘要](https://gitee.com/openatom-university/osstheory-fundamental/blob/master/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E7%BB%B4%E5%9F%BA%E7%BB%8F%E6%B5%8E%E5%AD%A6/%E5%AE%8F%E8%A7%82%E7%BB%B4%E5%9F%BA%E7%BB%8F%E6%B5%8E%E5%AD%A6%E6%91%98%E8%A6%81.md)

# 关于作者

- Don Tapscott是创新、媒体、技术的经济和社会影响方面的世界领先权威之一，为世界各地的企业和政府领导人提供咨询。
- Anthony D. Williams是New Paradigm的副总裁兼执行主编，撰写了大量有影响力的报告。
