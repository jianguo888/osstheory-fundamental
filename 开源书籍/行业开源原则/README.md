# 1. 简介

![输入图像信息](https://gitee.com/openatom-university/osstheory-fundamental/raw/develop/%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/Principles%20for%20Industrial%20Open%20Source/Principles%20for%20Industrial%20Open%20Source-18.png)

这本小册子介绍了如何管理开源转换的行业开源、行业验证和标准化模式。将其作为开源之旅的指南。特别是你会学到：

- 如何通过新的和可选择的收入来源创造更多的业务；
- 为什么贡献是至关重要的-确保为您的产品增加价值；
- 为什么遵从是必要的——作为你参与的入场券。

[报告下载](https://www.academia.edu/37365963/Principles_for_industrial_Open_Source)

# 2. 目录

## [01-行业开源的原则](https://gitee.com/openatom-university/osstheory-fundamental/blob/develop/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E8%A1%8C%E4%B8%9A%E5%BC%80%E6%BA%90%E5%8E%9F%E5%88%99/01-%E8%A1%8C%E4%B8%9A%E5%BC%80%E6%BA%90%E7%9A%84%E5%8E%9F%E5%88%99.md)
## [02-行业开源的模式](https://gitee.com/openatom-university/osstheory-fundamental/blob/develop/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E8%A1%8C%E4%B8%9A%E5%BC%80%E6%BA%90%E5%8E%9F%E5%88%99/02-%E8%A1%8C%E4%B8%9A%E5%BC%80%E6%BA%90%E7%9A%84%E6%A8%A1%E5%BC%8F.md)

# 3. 作者

|作者|公司和职务|
|---|---|
|Carl-Eric Mols|Head of Open Source Sony Mobile|
|Nicolás Martín-Vivaldi|SW Management Consultant Addalot|
|Magnus Ahlgren|SW Management Consultant Addalot|
|Morten Werther|SW Management Consultant Addalot|
|Krzysztof Wnuk|Associate Professor，Blekinge Institute of Technology|

# 4. 版权和许可

![输入图像信息](https://gitee.com/openatom-university/osstheory-fundamental/raw/develop/%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/CC%204.0-BY.jpg)
本作品在http://creativecommons.org/licenses/by/4.0/下获得许可
