# 《道路和桥梁》概述

[《道路和桥梁-我们数字基础设施背后的隐形劳动力》](https://www.fordfoundation.org/work/learning/research-reports/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure/)（Roads and Bridges: The Unseen Labor Behind Our Digital Infrastructure）是福特基金会的支持下于 2016 年发布的的报告。报告的作者Nadia Eghbal 探讨了公共代码缺乏机构支持的问题。她解开了系统当前的运作方式及其面临的独特挑战，并就如何解决问题提供了建议。

![输入图片说明](https://foruda.gitee.com/images/1666922724471747950/573a45e6_8894319.jpeg "Roads and Bridges1.jpeg")

正如 Eghbal 所述，数字基础设施应被视为必要的公共产品。免费的公共源代码使公司构建软件的成本指数级降低和更容易，并使技术在全球范围内更容易获得。然而，有一个普遍的误解，即开源项目的劳动力资金充足。实际上，它主要是由志愿者创建和维护的，他们这样做是为了建立自己的声誉，出于责任感，或者仅仅是出于爱心。公共编码社区的去中心化、非等级性质使得编码人员难以获得报酬，但由此产生的工作是数字资本主义经济的基础。越来越多的开发人员在不参与维护的情况下使用共享代码，

总体而言，补偿开发人员的长期解决方案没有得到足够的重视，那些获得有意义报酬的人认为自己是“幸运的”。当前数字基础设施的商业模式包括来自公司或个人的“赏金”、赞助或捐赠。尽管基金会、学术界和企业等机构在支持数字基础设施方面做出了一些努力，但这些努力还不够。尤其是公司没有被激励以全球公共编码项目所需的方式进行协作。

Eghbal 强调，由于开源依赖于人力而非财力资源，因此仅靠金钱并不能解决问题。需要对开源文化有细致入微的理解，以及管理而不是控制数字基础设施的方法。她建议资助和支持数字基础设施的努力包括去中心化，与现有软件社区合作，并提供长期、主动和全面的支持。提高对维持数字基础设施挑战的认识、让机构更容易贡献时间和金钱、扩大和多样化开源贡献者的池、制定跨基础设施项目的最佳实践和政策，都将大大有助于建立一个健康的和可持续的生态系统。

[报告下载地址](https://www.fordfoundation.org/media/2976/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure.pdf)

## 关于作者

Nadia Asparouhova（原名Nadia Eghbal）是一名作家和研究人员，她探索互联网如何使个人创作者成为可能。从2015年到2019年，她专注于开源软件的生产，在GitHub独立工作，改善开源开发者的体验。她是《道路与桥梁:数字基础设施背后看不见的劳动》一书的作者，该书由福特基金会出版，她认为开放源代码是一种需要维护的公共基础设施形式。

> “因为代码不如YouTube视频或Kickstarter活动那么有魅力，公众对这一工作的认识和欣赏很少。因此，对于引发信息革命的产出，几乎没有足够的制度支持。”

## 关于许可

[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

# 目录

## [第一章 前言](https://gitee.com/openatom-university/osstheory-fundamental/blob/master/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E9%81%93%E8%B7%AF%E4%B8%8E%E6%A1%A5%E6%A2%81/01-%E5%89%8D%E8%A8%80.md)
## [第二章 执行概要](https://gitee.com/openatom-university/osstheory-fundamental/blob/master/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E9%81%93%E8%B7%AF%E4%B8%8E%E6%A1%A5%E6%A2%81/02-%E6%89%A7%E8%A1%8C%E6%A6%82%E8%A6%81.md)
## [第三章 简介](https://gitee.com/openatom-university/osstheory-fundamental/blob/master/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E9%81%93%E8%B7%AF%E4%B8%8E%E6%A1%A5%E6%A2%81/03-%E7%AE%80%E4%BB%8B.md)
## [第四章 数字基础设施的历史和背景](https://gitee.com/openatom-university/osstheory-fundamental/blob/master/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E9%81%93%E8%B7%AF%E4%B8%8E%E6%A1%A5%E6%A2%81/04-%E6%95%B0%E5%AD%97%E5%9F%BA%E7%A1%80%E8%AE%BE%E6%96%BD%E7%9A%84%E5%8E%86%E5%8F%B2%E5%92%8C%E8%83%8C%E6%99%AF.md)
## [第五章 当前的体系是如何运作的](https://gitee.com/openatom-university/osstheory-fundamental/blob/master/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E9%81%93%E8%B7%AF%E4%B8%8E%E6%A1%A5%E6%A2%81/05-%E5%BD%93%E5%89%8D%E7%9A%84%E4%BD%93%E7%B3%BB%E6%98%AF%E5%A6%82%E4%BD%95%E8%BF%90%E4%BD%9C%E7%9A%84.md)
## [第六章 数字基础设施面临的挑战](https://gitee.com/openatom-university/osstheory-fundamental/blob/master/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E9%81%93%E8%B7%AF%E4%B8%8E%E6%A1%A5%E6%A2%81/06-%E6%95%B0%E5%AD%97%E5%9F%BA%E7%A1%80%E8%AE%BE%E6%96%BD%E9%9D%A2%E4%B8%B4%E7%9A%84%E6%8C%91%E6%88%98.md)
## [第七章 持续发展数字基础设施](https://gitee.com/openatom-university/osstheory-fundamental/blob/master/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E9%81%93%E8%B7%AF%E4%B8%8E%E6%A1%A5%E6%A2%81/07-%E6%8C%81%E7%BB%AD%E5%8F%91%E5%B1%95%E6%95%B0%E5%AD%97%E5%9F%BA%E7%A1%80%E8%AE%BE%E6%96%BD.md)
## [第八章 机会就在眼前](https://gitee.com/openatom-university/osstheory-fundamental/blob/master/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E9%81%93%E8%B7%AF%E4%B8%8E%E6%A1%A5%E6%A2%81/08-%E6%9C%BA%E4%BC%9A%E5%B0%B1%E5%9C%A8%E7%9C%BC%E5%89%8D.md)