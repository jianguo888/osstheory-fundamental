# 1. 摘要

![输入图片说明](https://foruda.gitee.com/images/1669251503787983905/673501b5_8894319.jpeg "LFResearch_AI_Data_Releasing_Internal_Code_Infographic_page-0001.jpg")

企业对开源的参与已经达到了历史最高水平，并且随着企业意识到消费和贡献开源项目的价值，这种参与还在继续增长。随着企业越来越多地发现开源专有技术可以创造新的价值来源和更强大的产品生态系统，企业参与的性质在不断演变。

开源是一项专有技术，涉及的远不止使源代码可用。有许多方法可以构建或加入社区来使用和帮助维护项目，这就是为什么它应该是一个有序和深思熟虑的过程。

对于计划将开源专有代码作为独立开源项目的公司，本文提供了该过程的高级概述，并提供了一个示例清单，可以帮助确保正确捕获和执行所有任务。

# 2. 简介

开源软件(OSS)已经将软件行业转变为一种新的范式，从关起门来开发专有代码，转变为开发各方可以公开共享、修改和重新分发的代码。这种转变的主要好处包括减少开发成本和软件组件复杂性，开发可重用的、通用的、现成的软件资产，增加灵活性，并从社区驱动的开发项目的创新倍增器因子中受益。将开源模型作为构建软件的积极手段的组织将增加他们保持竞争优势的机会。图1说明了OSS为采用它并对它做出贡献的组织提供的各种战略优势。

![输入图片说明](https://foruda.gitee.com/images/1669251803061824769/af3cc1c1_8894319.png "Picture2.png")

在过去的二十年中，组织已经意识到在其产品和服务中使用和贡献开源项目的好处。这创造了一种趋势，组织建立开放源码程序办公室(osos)来管理OSS的所有方面，包括使用和遵从OSS许可证，对OSS项目的贡献，以及围绕关键OSS技术的社区建设。

![输入图片说明](https://foruda.gitee.com/images/1669253033218967005/b04c3942_8894319.png "Screenshot from 2022-11-24 09-21-02.png")

图2说明了四种主要的OSS企业策略:OSS的消费、参与、贡献和领导。每一种战略都要求企业在前一种战略上取得成功，而组织的进步程度完全取决于企业本身。工程驱动了消费和参与的早期策略。工程师使用各种开放源码组件来提高开发速度，但是他们很少参与维护这些组件的项目。随着时间的推移，组织的高层会了解这种OSS使用的价值。随着OSS的发展，业务需求开始推动此类OSS的参与，而OSS的努力有助于确定业务策略。一些公司实现了他们作为消费者的目标。其他公司在其他参与阶段看到了战略优势，在大多数情况下，他们建立了一个OSPO来监督这些阶段的战略规划和执行。

作为第三阶段的一部分——为开源贡献力量——组织通常会出于各种动机选择为开源贡献关键的专有技术，例如:

- 提供标准的参考实现
- 确保关键软件保持可用性
- 削弱竞争
- 市场商品化
- 与他人合作，促进开发者社区的良好信誉
- 通过构建生态系统驱动市场需求
- 为客户提供支持和添加自定义功能的能力

带有错误动机的开源通常会对实现预期结果产生负面影响，并可能破坏企业与特定开源项目社区的关系。

本文确定了在使专有技术开源时要问的问题、要考虑的实践和要采取的步骤。图3说明了开放源码内部代码和将其作为开放源码项目启动过程中涉及的各个步骤。这些步骤不一定以线性顺序执行，其中几个步骤可以并行进行。本文的目标是提供一个基本模板，组织可以对其进行调整，以适应他们自己的策略和策略。

![输入图片说明](https://foruda.gitee.com/images/1669253054553450519/54a7d84b_8894319.png "Screenshot from 2022-11-24 09-21-20.png")

# 3. 初步调查

当开源专有技术时，重要的是彻底评估转变的原因，并相应地调整内部激励和度量。出于错误原因的开源可能会产生与最初预期相反的效果。要成功地开源一个项目，您必须有正确的理由或动机。

## 从商业角度考虑开源

开放源代码自有代码有许多合理的商业理由，例如:

- 强化你正在构建的产品或服务的生态系统
- 通过与业务合作伙伴和客户一起增强功能和修复bug来提高产品质量
- 提供一个标准的参考实现，从而驱动你的软件作为事实上的实现被采用
- 商品化软件堆栈的非战略层
- 推动更高的价值线，推动更多的创新
- 与开源社区合作，并在开发人员社区中增加善意

同样，开放专有代码也有许多适得其反的原因。这些论点应该作为危险信号:

- 你希望其他人维护你仍然需要的代码库，这样你就可以停止在代码上投资
- 你想要退休代码的独特功能，你将不再维护或在你的产品中使用
- 源代码直接链接到在开放源码许可下不能发布的代码

现在您已经有了在组织中开放源代码的业务案例，下一步是确定开放源代码的实际路径。

## 评估开源的可能方式

实现可能的目标没有单一的方法，这也不是您的组织必须单独完成的任务。在大多数情况下，有多个选项供您探索，例如:

- 评估技术，并决定是否应该开源任何其他组件
- 分析你的公司可以作为主要参与者加入的现有开源项目，减少创建新基础设施和新社区的需要
- 探索与现有客户和合作伙伴启动计划中的开源项目的可能性
- 评估在一个有成功启动和维持开源项目记录的开源基金会中启动和托管计划中的开源项目的选项。

# 4. 项目资金

一旦您制定了开源的业务案例，您将需要一个项目计划和时间阶段的预算，包括启动和维护项目的成本。有些成本是一次性的，有些则是经常性的。这类费用的例子如下:

- 内部法律努力导致公开发布代码
- 正在进行的IT项目基础设施和云计算(如果适用)
- 商标管理
- 创意和品牌推广(标志，网站，活动标牌等)
- 项目管理
- 定期进行开放源码许可合规和安全扫描
- 社区活动和黑客马拉松

## 法律方面的考虑

在法律方面，有五项主要活动需要执行。这些包括:

- 确认用于开放源代码的所有源代码的所有权
- 进行知识产权审查
- 选择开放源码许可证
- 将许可条款应用于代码并更新源代码中的许可信息
- 在公开发布源代码之前清理源代码


在下面的小节中，我们将介绍这些不同的活动，并在适用的情况下提供建议。

## 确认所有代码的所有权

开源专有技术的风险之一是偶然包含第三方专有代码作为开源代码的一部分。在发布开源许可下的任何代码之前，强烈建议组织拥有开源代码所需的所有权利和权限。

本练习的一些步骤包括:

- 使用软件组合分析(SCA)工具1审核源代码
- 识别第三方源代码——开源或商业代码
- 确定您是否有权在开源许可下对任何已发现的第三方商业代码进行开源;如果答案是否定的，那么您不能开源某些第三方代码，那么您需要提供替代代码

## 进行知识产权审查

可能受到专利或其他知识产权要求的软件并不是开源发布的理想候选。这里有一些问题可以帮助你完成这个实践:

- 代码是否披露或实现了公司计划通过专利保护的任何发明?如果答案是肯定的，那么你需要决定是否删除代码，建立IP策略，或做出非断言承诺。在所有情况下，当出现这种情况时，您的法律顾问将为下一步的步骤提出适当的建议。
- 源代码发布会引发针对开源软件的专利索赔吗?如果是，则需要删除和替换受保护代码，或者寻求合适的许可或权限。
- 您为开源项目选择的名称(假设您正在启动一个新项目)是否受商标法保护?该名称或任何与项目相关或注册的商标是否存在任何侵权索赔风险?

## 选择开放源码许可证

开源项目的许可证决定了使用、复制、修改和分发代码的权利。开源项目的许可证选择是决定项目开放程度的一个重要因素。开源项目应该只使用开源计划已经批准的许可证。这样的许可证允许自由使用、修改和共享软件。要得到开源计划的批准，必须获得许可证

通过它的许可证审查过程来确认许可证满足它的开源定义(OSD)。您可能会遇到许多与OSD不兼容的其他许可证。这些许可大多数是“可用源代码”许可，通常包括对软件的使用和/或分发的限制或限制。这些限制常常导致许可证与OSD不兼容。

总是建议采用osi批准的开放源码许可证。具体许可证的选择取决于您作为一个组织想要实现的具体目标。我们在本节中预览一些问题，这些问题将推动对这个主题的讨论，并帮助您就采用的许可证做出决定。

- 您想放弃对其他人如何使用和分发代码的控制吗?
- 您希望允许其他人在商业程序和产品中使用代码吗?
- 如果其他人在他们的程序中使用代码并出售它来赚钱，你想要这些收入的一部分吗?
- 如果其他人使用和分发代码并改进它(例如，修复bug或添加功能)，您希望他们将这些改进贡献给项目吗?你会接受什么样的许可来提交代码?
- 您是否需要开发人员原产地证书(DCO)或贡献者许可协议(CLA)作为贡献要求?

作为这项工作的一部分，可能还有其他问题需要考虑，这主要是由您的法律顾问和您组织内的技术领导者驱动的。

**DCO** 

DCO签名过程确保项目接受的每一行代码都有明确的审计跟踪。这是一种开发人员的证明，证明他们有权将代码提交到项目中。例如，Linux内核进程要求所有贡献者签署他们的代码，这表明贡献者对代码进行了认证，如DCO中所述。签名表明贡献者已经在适当的开源许可下创建或接收了贡献，该许可根据文件中指示的许可将其合并到项目的代码库中。DCO建立了一个负责许可和证明对项目贡献的人员链。

 **CLA** 

有些项目要求开发人员或其雇主签署CLA。与DCO不同的是，CLA的文本在不同的项目之间可能有很大的不同，因此任何给定CLA的条款可能具有不同的效果。CLA的目的是确保项目输出的监护人对所有贡献拥有必要的所有权或授予的权利，以允许它们在所选的许可下分发。在某些情况下，这甚至意味着贡献者将授予不可撤销的许可，这允许项目将贡献作为项目的一部分分发。

## 将许可条款应用到代码中

一旦按照上一步中提供的建议清理了源代码，就可以将许可条款应用到代码中了。这个练习包括以下步骤:

- 在每个源代码文件中添加许可头和版权声明
- 在项目的网站上明确标明许可证，你提供的任何常见问题(FAQ)，以及适用的下载页面
- 在源文件中使用SPDX2 License List“短标识符”

## 代码清理

开源项目的另一个风险是包含私人信息、机密通信和商业秘密。为了降低这种风险，你可以采取以下措施:

- 删除任何非公共应用程序编程接口(api)的公开。
- 删除任何包含员工姓名或个人身份信息、产品代号、路线图、未来产品描述或贬损的评论。
- 从源代码中删除任何未使用或过时的代码，以增加社区做出贡献的可能性。
- 创建并包含一个文件，其中包含所有第三方软件的许可和版权声明，如果适用，使源代码可用。
- 如果源代码依赖于第三方代码，那么向社区提供必要的信息;最好代码不依赖于任何非开源组件。
- 删除任何第三方专有代码。
- 在组件根目录下添加license文件，包含完整的license文本。例如，如果你在GitHub上发布代码，你需要提供一个许可证。Md文件，包含开源许可证的全文

## 项目品牌

项目品牌化包括许多应该考虑的活动，并将在以下小节中讨论。

1）制定商标战略和政策

- 对项目的名称/标记达成一致。
- 执行淘汰制搜索以确定注册是否成功。
- 指定商标的内部联系人(如果不是律师)。
- 制定注册策略:适用哪类商品/服务?优先考虑哪些国家?
- 注册商标名称和商标。

2）域名

- 注册域名并设置重定向。

3）创造性的资产

- 创建logo、logo包和视觉资产。
- 创建并在网站上发布logo使用指南。

4）注册外部帐户

- 在GitHub上设置项目组织名称。
- 在各种社交媒体平台上建立@projectname，如Twitter, LinkedIn和Facebook。

5）制定认证/遵从策略

- 确定项目必须满足的标准，以声明与父项目的兼容性。
- 创建一个规范文档和工具，以验证项目的自定义构建是否符合规范。
- 对认证项目的名称/标志达成一致。
- 如果你希望控制项目名称的使用，制定商标政策/常见问题解答。问这些问题来推动话题:分销商、用户组或开发者可以注册包含项目名称的域名吗?项目是否运行认证程序，允许其他人将商标用于修改后的产品?
- 创建一个认证测试套件。
- 与测试机构建立合同。
- 计划第一年的插件节。

6）招募商业伙伴

- 在项目启动当天，向将从项目中获益最多的商业合作伙伴寻求公众支持。
- 获得关键合作伙伴的承诺，鼓励员工参与项目，并有一些基本的承诺。
- 接近兼容的项目，沟通新项目将如何使他们受益，并为宣布做好准备。
- 预测现有项目将启动误解为竞争的冲突，并在启动前化解冲突。
- 让业务合作伙伴尽早访问项目源代码。
- 与合作伙伴一起为共享客户建立共同的价值主张和参考堆栈。

## 建立项目治理

治理确定谁对项目具有超出开放源码许可证中合法要求的影响力和控制权。项目的治理模型建立了一个协作框架，该框架解决了一些困难的问题，例如:

1）贡献

- 谁决定代码的包含和发布，如何决定?
- 谁可以担任项目的主要维护者或架构师(较大的项目有不止一个)?
- 项目贡献者如何成为维护者或提交者?

2）方向与财务

- 项目如何筹集资金，谁决定如何使用这些资金?
- 项目是否应该有技术指导委员会(TSC)或符合性和认证委员会?谁能在上面?
- 谁来决定项目的方向和路线图?

3）透明

- 谁可以参与讨论并决定关键问题?
- 决策过程的透明度如何?
- 任何人都能跟上项目中发生的讨论和会议吗?

4）重用

- 重新分发、修改或使用软件的遵从性要求是什么?
- 项目如何使贡献者和下游再分销商遵守这些要求?

5）版权和商标

- 谁拥有所贡献代码的版权?
- 用户如何许可项目的品牌?

通常，项目的初始维护者组成项目的TSC。这些人可能来自项目的创始机构。目标是随着时间的推移使TSC增长，以包括高价值的贡献者。

6）项目管理

- 确定TSC的成员。
- 确定TSC的主要职责，例如:
    - 监督软件架构和实现活动
    - 起草发布计划和路线图
    - 与新项目所依赖的其他开源项目合作
    - 设置接受/拒绝代码的标准
    - 管理源代码安全问题

7）项目流程

高度开放的项目将有明确定义的流程，包括如何在社区中工作以及如何为项目做出贡献。对于初学者来说，一个清晰的开发过程应该概述如何将代码整合到项目中，项目的发布过程和进度，以及开发人员需要满足的任何要求才能让他们的代码被接受。这还应该包括参与的指导方针，演示社区的最佳实践，如补丁提交、特性请求、bug报告和停止代码贡献。

- 特性要求
- 发布管理
- 代码提交
- Bug报告

8）项目协议

- 制定第三方贡献协议，以规范项目如何管理来自社区的贡献。

## 建立项目基础设施

- 文档
- 项目网站
- 维基
- 社区沟通渠道
    - 邮件列表
    - 实时聊天(例如，Slack, Internet中继聊天(IRC))
- 合作平台
    - 维基
    - GitHub仓库(或管理自己的git服务器)
- Bug跟踪和特性请求
- 构建系统

## 为你的GitHub仓库应用推荐的实践

- 使用TODO组创建的REPOLINTER工具来识别GitHub回购中的常见问题。
- 使用双重身份验证保护您的GitHub帐户。
- 确保每个仓库都包含LICENSE文件。
- 在您的仓库中添加一个README文件，欢迎新的社区成员加入项目，并解释为什么项目很有用以及如何开始。
- 向你的仓库添加一个贡献文件，解释如何向其他开发人员和你的
- 用户社区。在较高的级别上，该文件将解释需要什么类型的贡献以及流程如何工作。
- 添加CODEOWNERS文件来定义存储库中负责代码的个人或团队。
- 添加一个CODE _ OF _ CONDUCT文件，为参与者的行为设置基本规则，有助于促进一个友好、欢迎的环境。虽然不是每个项目都有一个code_ OF _ CONDUCT文件，但它的存在表明这是一个受欢迎的项目，并定义了与项目社区接触的标准。
- 提供关于发布方法、节奏、标准等的文档。
- 记录您的项目治理，并使其在项目的回购中可用。
- 添加一个SUPPORT文件，让用户和开发人员知道如何获得项目的帮助。你可以加上how和这个文件处理安全问题，把它放在项目的顶层README中，或者参考安全文档。
- 设置问题模板和拉请求模板，以帮助您定制和标准化希望贡献者在存储库中打开问题和拉请求时包含的信息。
- 获得并保持项目的OpenSSF最佳实践徽章(以前称为核心基础设施计划最佳实践徽章)。
- 确定由谁来处理安全问题(可以是一个团队)，并建立一个单独的电子邮件帐户。
- 考虑让项目成为一个CNA (CVE编号机构)。
- 尽可能在仓库中每个文件顶部的注释中包含SPDX简写标识符。
- 采用GitHub DCO应用程序在每次提交时强制使用“Signed off-by:”标签。对于贡献者来说，DCO是一种简单的方法，可以证明他们编写了代码，或者有权向项目提交他们所贡献的代码。应用程序在Pull Requests上强制DCO。它要求所有提交消息都包含signed -off行和与提交作者匹配的电子邮件地址。
- 使用英语作为你在GitHub上发布的任何内容的默认通用语言。你可以支持第二语言，但英语应该是面向全球用户交流的主要语言。

# 5. 项目启动

## 准备公告

- 向发行合作伙伴介绍情况。
- 检查所有的项目基础设施是运行的，安全的，可扩展的。
- 将项目关键人员订阅到项目邮件列表。
- 确保内部开发人员加入并持续监控实时聊天。

媒体和分析师关系

- 制定发布策略和时间表。
- 起草新闻稿，并获得相关各方的签字。
- 确定发言人和媒体联系人。
- 创建内部和外部的FAQ。

# 6. 运行开源项目的推荐实践摘要

1）许可证

- osi批准的开源许可证提供了创建和发布衍生产品的自由。

2）治理

- 一个对项目的所有当前和未来贡献者给予平等地位的治理模型。具有开放和透明治理模型的开源项目有更好的发展机会，拥有健康的环境，并吸引开发人员和采用率。

3）访问

- 任何对项目感兴趣的用户或开发人员都可以访问项目资源。任何人都可以参与项目，任何参与者都可以通过贡献和与项目社区建立信任来获得提交者权利。

4）流程

- 为请求特性、报告bug、提交代码等记录一般的项目过程。
- 源代码贡献只通过项目定义的流程提交。
- 所有代码都要经过同行评审过程。
- 成为提交者/维护者/审核者的过程由项目强制执行，以保持一致性。
- 项目社区根据收到的反馈修改其流程，以确保随着项目的发展和规模，他们继续满足项目的需求。

5）发展

- 开发的责任分配给最有能力完成任务的个人。
- 项目在合并代码时强制执行质量标准。
- 在进入最终版本之前，项目执行多级评审。
- 同行评审是强制性和公开的。

6）社区

- 对新来者开放——开放开发通常力求包容性。
- 关注能见度，强调开放的决策过程和沟通。
- 自组织;个人在自己或雇主感兴趣的领域做出贡献。
- 考虑到领导力需要经验，对组织变革有弹性。如果个人停止参与，就会有其他人接替他们的位置。

7）社区结构

- 精英制度推动进步和接受。为社区提供最大价值的贡献者被授予项目领导角色。
- 该项目欢迎有自由和权限参与公共讨论、开发和测试的新成员。
- 管理持续的媒体和分析师关系。
- 发展持续的公共关系/分析师关系战略。
- 如果需要的话，与PR/AR公司合作以完全实现战略。

8）宣布并启动项目

- 发布源代码。
- 发布路线图，即使是初步的。
- 遵循开源开发模式。
- 跨接触点监测PR/AR策略的效果。
- 项目的层次结构是可伸缩的，因为它由维护人员组成，他们监督特定的代码体的级别，可以根据社区的大小根据需要添加或删除。
- 任何人都可以提交补丁，开发人员和用户都可以参与到测试过程中。开发人员和用户的角色与开源开发紧密结合，允许用户有一个更直接的途径来影响项目。

7）发布

- 为了保护某些用户不受快速开发软件的不稳定性影响，项目提供了稳定的版本，限制了实验性特性的添加，以提供更好地支持依赖于稳定性的用例的可靠版本。
- 每周或每月的稳定版本在经过测试后为用户和开发人员提供最新的功能。
- 长期稳定的版本扩展到更长的时间，通常只包括安全补丁和错误修复。
- 项目有固定的发布节奏，每个发布都有固定的目标。
- 所有项目涉众都知道每个版本的发布节奏和要达到的目标。

8）通信工具

- 包括邮件列表、Slack和IRC在内的工具对任何希望参与该项目的人都是开放的。

9）透明度

- 开源社区必须尽可能透明，以吸引新的参与者，例如贡献透明、同行评审透明、讨论透明以及对提交者或维护者的推广透明。

10）文档

- 提供架构、api、安装指南、开发人员指南、开发过程、参与指南、教程等文档。
 
 _报告来源：Linux Foundation Research_ 
 _[报告下载地址](https://project.linuxfoundation.org/hubfs/LF%20Research/LFResearch_AI_Data_Releasing_Internal_Code_Report.pdf?hsLang=en)_ 
_Copyright © 2022 The Linux Foundation | September 2022_
 
![CC4.0](https://foruda.gitee.com/images/1666681568689981505/0ecfaebc_8894319.png "by-nd (2).png")